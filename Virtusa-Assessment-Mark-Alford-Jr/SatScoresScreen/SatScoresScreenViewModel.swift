//
//  SatScoresScreenViewModel.swift
//  Virtusa-Assessment-Mark-Alford-Jr
//
//  Created by Mark Alford on 3/15/23.
//

import Foundation

class SatScoresScreenViewModel {
    
    init() {}
    //MARK: - Network Call
    /// used in the HomeScreenViewController to handle populating the tableview data array with the api JSON data
    var updateUI: ((_ newDataToDisplay: SATScoreModelArray?) -> Void)?
    /// API request call, will be handled in the VC, using the updateUI closure
    func requestData(dbnName: String) {
        let endpoint = SATScoresAPI.getSatScore(dbn: dbnName)
        NetworkManager.request(endpoint: endpoint) { [weak self] (result: Result<SATScoreModelArray, Error>) in
            switch result {
            case .success(let response):
                DispatchQueue.main.async {
                    self?.updateUI?(response)
                }
            case .failure(let error):
                print("sat score failed: \(error)")
            }
        }
    }
}
