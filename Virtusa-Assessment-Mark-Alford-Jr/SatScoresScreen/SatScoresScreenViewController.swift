//
//  SatScoresScreenViewController.swift
//  Virtusa-Assessment-Mark-Alford-Jr
//
//  Created by Mark Alford on 3/15/23.
//

import UIKit

class SatScoresScreenViewController: UIViewController {

    let viewModel = SatScoresScreenViewModel()
    
    //MARK: - UI Components
    var schoolNameText: UILabel = {
        let text = UILabel()
        text.text = "School Name High"
        text.textAlignment = .center
        text.font = UIFont.boldSystemFont(ofSize: 24)
        text.numberOfLines = 0
        return text
    }()
    
    var mathScoreText: UILabel = {
        let text = UILabel()
        text.text = "Math Score"
        text.textAlignment = .center
        text.font = UIFont.systemFont(ofSize: 28, weight: UIFont.Weight.medium)
        text.numberOfLines = 0
        return text
    }()
    var writingScoreText: UILabel = {
        let text = UILabel()
        text.text = "Writing Score"
        text.textAlignment = .center
        text.font = UIFont.systemFont(ofSize: 28, weight: UIFont.Weight.medium)
        text.numberOfLines = 0
        return text
    }()
    var readingScoreText: UILabel = {
        let text = UILabel()
        text.text = "Reading Score"
        text.textAlignment = .center
        text.font = UIFont.systemFont(ofSize: 28, weight: UIFont.Weight.medium)
        text.numberOfLines = 0
        return text
    }()
    
    //MARK: - Initializers
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        view.backgroundColor = UIColor.systemBackground
        view.addSubview(schoolNameText)
        view.addSubview(mathScoreText)
        view.addSubview(writingScoreText)
        view.addSubview(readingScoreText)
        viewModel.updateUI = { [weak self] data in
            DispatchQueue.main.async {
                self?.configureUI(school: data?.first?.schoolName ?? "No School Availble", math: data?.first?.satMathAvgScore ?? "No Math Score Availble", writing: data?.first?.satWritingAvgScore ?? "No Writing Score Availble", reading: data?.first?.satCriticalReadingAvgScore ?? "No Reading Score Availble")
            }
        }
        subViewContraints()
    }
    
    //MARK: - Network Data Handling
    /**
     Populates the SATScore API call with the neccessary JSON data from the School API
     - Parameters:
        - dbnName: the unique dbn string of EACH school
        - school: fills the navigation title up, when the School API has no SAT score data
     */
    init(dbnName: String, school: String) {
        super.init(nibName: nil, bundle: nil)
        navigationController?.title = school
        viewModel.requestData(dbnName: dbnName)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Constraints
    /// sets up the constraints of all UI in this file
    func subViewContraints() {
        schoolNameText.translatesAutoresizingMaskIntoConstraints = false
        mathScoreText.translatesAutoresizingMaskIntoConstraints = false
        writingScoreText.translatesAutoresizingMaskIntoConstraints = false
        readingScoreText.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            schoolNameText.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            schoolNameText.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -32),
            schoolNameText.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24),
            
            mathScoreText.topAnchor.constraint(equalTo: schoolNameText.bottomAnchor, constant: 16),
            mathScoreText.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            writingScoreText.topAnchor.constraint(equalTo: mathScoreText.bottomAnchor, constant: 8),
            writingScoreText.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            readingScoreText.topAnchor.constraint(equalTo: writingScoreText.bottomAnchor, constant: 8),
            readingScoreText.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
    }
    
    //MARK: - UI Configuration
    /**
     fills this ViewController's UI properties with data from an API call. the API data comes from whichever class is calling this VC
     - Parameters:
        - school: the JSON response item's school name
        - math: the JSON response item's math score
        - writing: the JSON response item's writing score
        - reading: the JSON response item's reading comprehension score
     */
    func configureUI(school: String, math: String, writing: String, reading: String) {
        schoolNameText.text = school
        mathScoreText.text = math
        writingScoreText.text = writing
        readingScoreText.text = reading
    }
}
