//
//  HomeScreenViewModel.swift
//  Virtusa-Assessment-Mark-Alford-Jr
//
//  Created by Mark Alford on 3/15/23.
//

import Foundation
import UIKit

class HomeScreenViewModel {
    
    //MARK: - Network Call
    init() {}
    
    let endpoint = SchoolAPI.getschools
    var tableData: SchoolModelArray = []
    /// used in the HomeScreenViewController to handle populating the tableview data array with the api JSON data
    var updateUI: ((_ newDataToDisplay: SchoolModelArray?) -> Void)?
    
    /// API request call, will be handled in the VC, using the updateUI closure
    func fetchRequestData() {
        NetworkManager.request(endpoint: endpoint) { [weak self] (result: Result<SchoolModelArray, Error>) in
            switch result {
            case .success(let response):
                DispatchQueue.main.async {
                    self?.tableData = response
                    self?.updateUI?(self?.tableData)
                    // testing the JSON
                    print(response.first?.dbn)
                    print(response.first?.schoolName)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
}

